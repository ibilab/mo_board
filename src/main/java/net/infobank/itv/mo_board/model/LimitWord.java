package net.infobank.itv.mo_board.model;

import lombok.Data;

@Data
public class LimitWord {

	private int lword_key;
	private int pgm_key;
	private int ch_key;
	private String mo_word;
	private String word_limt;
	private String word_replace;
	private int word_use;

}
