package net.infobank.itv.mo_board.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.infobank.itv.mo_common.model.Schedule;

public class ScheduleDAO {
    private static Logger log = LoggerFactory.getLogger(ScheduleDAO.class);
	private SqlSessionFactory sqlSessionFactory = null;

	public ScheduleDAO(SqlSessionFactory sqlSessionFactory){
        this.sqlSessionFactory = sqlSessionFactory;
    }

	public List<Schedule> selectAll() {
		List<Schedule> list = new ArrayList<Schedule>();
		SqlSession session = sqlSessionFactory.openSession();

		try {
			list = session.selectList("Schedule.selectAll");
        } catch(Exception e)  {
            log.error("Schedule.selectAll : " , e);			
		} finally {
			session.close();
		}

		return list;
	}

	public List<Schedule> selectMoSchedule() {
		List<Schedule> list = new ArrayList<Schedule>();
		SqlSession session = sqlSessionFactory.openSession();

		try {
			list = session.selectList("Schedule.selectMoSchedule");
		} catch(Exception e)  {
		    log.error("Schedule.selectMoSchedule : " , e);
		} finally {
			session.close();
		}

		return list;
	}

	public List<Schedule> selectSchedule() {
		List<Schedule> list = new ArrayList<Schedule>();
		SqlSession session = sqlSessionFactory.openSession();

		try {
			list = session.selectList("Schedule.selectSchedule");
        } catch(Exception e)  {
            log.error("Schedule.selectSchedule : " , e);			
		} finally {
			session.close();
		}

		return list;
	}

    public Schedule selectById(int id) {
    	Schedule schedule = null;
        SqlSession session = sqlSessionFactory.openSession();
        try {
        	schedule = session.selectOne("Schedule.selectById", id);
        } catch(Exception e)  {
            log.error("Schedule.selectById : " , e);        	
        } finally {
            session.close();
        }

        return schedule;
    }
    public int insert(Schedule schedule) {
        int id = -1;
        SqlSession session = sqlSessionFactory.openSession();

        try {
            id = session.insert("Schedule.insert", schedule);
            session.commit();
        } catch(Exception e)  {
            log.error("Schedule.insert : " , e);            
            session.rollback();
        } finally {
            session.close();
        }

        return id;
    }

    public void update(Schedule schedule){

        SqlSession session = sqlSessionFactory.openSession();

        try {
            session.update("Schedule.update", schedule);
            session.commit();
        } catch(Exception e)  {
            log.error("Schedule.update : " , e);
            session.rollback();
        } finally {
            session.close();
        }
    }

    public void delete(int id){

        SqlSession session = sqlSessionFactory.openSession();

        try {
            session.delete("Schedule.delete", id);
            session.commit();
        } catch(Exception e)  {
            log.error("Schedule.delete : " , e);
            session.rollback();
        } finally {
            session.close();
        }
    }
}
