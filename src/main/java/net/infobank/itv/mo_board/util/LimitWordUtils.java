package net.infobank.itv.mo_board.util;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.infobank.itv.mo_board.dao.LimirWordDAO;
import net.infobank.itv.mo_board.model.LimitWord;
import net.infobank.itv.mo_common.util.MyBatisConnectionFactory;

public class LimitWordUtils {
    private static Logger log = LoggerFactory.getLogger(LimitWordUtils.class);
    

     
    public static List<LimitWord> getLimitWord() {
        LimirWordDAO limitwordDAO = new LimirWordDAO(MyBatisConnectionFactory.getSqlSessionFactory());
        List<LimitWord> list = new ArrayList<LimitWord>();
        try {
            list = limitwordDAO.selectLimitWord();
        } catch (Exception e) {
            log.error("Exception  getLimitWord : ", e);
        }
        return list;
    }
    
    public static List<LimitWord> getLimitWord_2() {
        LimirWordDAO limitwordDAO = new LimirWordDAO(MyBatisConnectionFactory.getSqlSessionFactory());
        List<LimitWord> list = new ArrayList<LimitWord>();
        try {
            list = limitwordDAO.selectLimitWord_2();
        } catch (Exception e) {
            log.error("Exception  getLimitWord_2 : ", e);
        }
        return list;
    }
    
    
    public static int getLimitUser(int pgm_key , String msg_com , String msg_userid) {
        LimirWordDAO limitwordDAO = new LimirWordDAO(MyBatisConnectionFactory.getSqlSessionFactory());
        int msg_show = 1;
        
        try {
        	String userTable = "user_";
        	Integer toPgm = pgm_key;
        	//String stoPgm = toPgm.toString();
        	userTable += toPgm.toString();
        	
        	
        	
        	msg_show = limitwordDAO.selectLimitUser(userTable , msg_com , msg_userid);
        } catch (Exception e) {
            log.error("Exception  getLimitUser : ", e);
        }
        return msg_show;
    }
    
    public static int getLimitUser_2(int pgm_key ,  String msg_userid) {
        LimirWordDAO limitwordDAO = new LimirWordDAO(MyBatisConnectionFactory.getSqlSessionFactory());
        int msg_show = 1;
        
        try {
        	String userTable = "user_";
        	Integer toPgm = pgm_key;
        	//String stoPgm = toPgm.toString();
        	userTable += toPgm.toString();
        	
        	
        	
        	msg_show = limitwordDAO.selectLimitUser_2(userTable ,  msg_userid);
        } catch (Exception e) {
            log.error("Exception  getLimitUser_2 : ", e);
        }
        return msg_show;
    }

}
