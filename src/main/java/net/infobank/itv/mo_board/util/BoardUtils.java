package net.infobank.itv.mo_board.util;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.infobank.itv.mo_board.dao.BoardDAO;
import net.infobank.itv.mo_common.model.ProgramMsg;
import net.infobank.itv.mo_common.util.MyBatisConnectionFactory;

public class BoardUtils {
    private static Logger log = LoggerFactory.getLogger(BoardUtils.class);
    

     
    public static int bulkInsert(List<ProgramMsg> list) {
        int id = -1;
        BoardDAO boardDAO = new BoardDAO(MyBatisConnectionFactory.getSqlSessionFactory());

        try {
        	id = boardDAO.bulkInsert(list);
        } catch(Exception e)  {
            log.error("Board.bulkinsert : " , e);            
        }

        return id;
    }
    
    public static int bulkInsert_2(List<ProgramMsg> list) {
        int id = -1;
        BoardDAO boardDAO = new BoardDAO(MyBatisConnectionFactory.getSqlSessionFactory());

        try {
        	id = boardDAO.bulkInsert_2(list);
        } catch(Exception e)  {
            log.error("Board.bulkinsert_2 : " , e);            
        }

        return id;
    }
    

}
