package net.infobank.itv.mo_board.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.infobank.itv.mo_common.model.ProgramMsg;

public class BoardDAO {
    private static Logger log = LoggerFactory.getLogger(BoardDAO.class);
	private SqlSessionFactory sqlSessionFactory = null;

	public BoardDAO(SqlSessionFactory sqlSessionFactory){
        this.sqlSessionFactory = sqlSessionFactory;
    }


	
    public int bulkInsert(List<ProgramMsg> list) {
        int id = -1;
        SqlSession session = sqlSessionFactory.openSession();

        try {
        	
        	/*
        	Map<String, Object> insertMap = new HashMap<String, Object>();
        	insertMap.put("ProgramMsg", list);
        	*/
        
            id = session.insert("Board.bulkinsert", list);
            session.commit();
        } catch(Exception e)  {
            log.error("Board.bulkinsert : " , e);
            session.rollback();
        } finally {
            session.close();
        }

        return id;
    }
    
    public int bulkInsert_2(List<ProgramMsg> list) {
        int id = -1;
        SqlSession session = sqlSessionFactory.openSession();

        try {
        	
        	/*
        	Map<String, Object> insertMap = new HashMap<String, Object>();
        	insertMap.put("ProgramMsg", list);
        	*/
        
            id = session.insert("Board.bulkinsert_2", list);
            session.commit();
        } catch(Exception e)  {
            log.error("Board.bulkinsert_2 : " , e);
            session.rollback();
        } finally {
            session.close();
        }

        return id;
    }


}
