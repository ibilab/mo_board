package net.infobank.itv.mo_board.kafka;

import java.io.StringReader;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.WakeupException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import net.infobank.itv.mo_board.model.LimitWord;
import net.infobank.itv.mo_board.util.BoardUtils;
import net.infobank.itv.mo_board.util.LimitWordUtils;
import net.infobank.itv.mo_board.util.ScheduleUtils;
import net.infobank.itv.mo_common.model.ConsumerInfo;
import net.infobank.itv.mo_common.model.ProgramMsg;
import net.infobank.itv.mo_common.util.CommonUtils;

public class ConsumerThread implements Runnable {
    private static Logger log = LoggerFactory.getLogger(ConsumerThread.class);

    String BootStrapServer;
    ArrayList<String> Monum;
    String Pre_Groupname;
    String Web_Root;
    String Web_Dir; 
    int RefreshTimer;
    int ReLoad_Sec;
    
    // Sub Consumer
    KafkaConsumer<String, String> consumer = null;
    private Map<TopicPartition, OffsetAndMetadata> currentOffsets = new HashMap<>();

    private final AtomicBoolean running = new AtomicBoolean(false);

    public ConsumerThread(ConsumerInfo Info) {
        // store parameter for later user
        BootStrapServer = Info.getBoot_strap_server();
        Monum = Info.getMo_num();
        Pre_Groupname = Info.getPre_group_name();
        ReLoad_Sec = Info.getReload_sec();
        Web_Root = Info.getWeb_root();
        Web_Dir = Info.getWeb_dir();
    }

    public void shutdown() {
        log.info("Stop this thread");
        running.set(false);
    }

    private class HandleRebalance implements ConsumerRebalanceListener {
        public void onPartitionsAssigned(Collection<TopicPartition> partitions) { 
        }

    	public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
    	    log.info("Lost partitions in rebalance. " +
    	            "Committing current offsets:" + currentOffsets);
    	    consumer.commitSync(currentOffsets);
    	
    	    // Reblanacing error add 
    	    currentOffsets.clear(); // 추가 코드
	    }
    	
    }
    
	@Override
    public void run() {
        Thread shutDownHook = new ShutdownHook(Thread.currentThread(), "Shutdown");
        Runtime.getRuntime().addShutdownHook(shutDownHook);
        
        log.info("run thread : " + Thread.currentThread().getName());

        String group;
        String message = null;
        Gson g = new Gson();
        int gapTime = 0;
        int currentTime = 0;
        List<ProgramMsg> programMsg_list = new ArrayList<ProgramMsg>();

        // init Reload time
        currentTime = (int) (System.currentTimeMillis() / 1000);
        gapTime = currentTime;

        // 쓰레드 스탑
        running.set(true);

        // Get limitword
        List<LimitWord> limitword_list = LimitWordUtils.getLimitWord();

        // sUB sETTING
        group = Pre_Groupname;
        log.info("groupname : " + group);

        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BootStrapServer);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, group);
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
        props.put(ConsumerConfig.ALLOW_AUTO_CREATE_TOPICS_CONFIG, false);
        props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "30000");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        consumer = new KafkaConsumer<String, String>(props);

        
        try {
            consumer.subscribe(Monum, new HandleRebalance());
            //insertTopic(Monum);

        	while (running.get()) {
          	
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
                
                for (ConsumerRecord<String, String> record : records) {
                    try {
                        message = record.value();
                        
                        log.info("topic = {}, partition = {}, offset = {}, key = {}, msg = {}",
                                record.topic(), record.partition(), record.offset(),
                                record.key(), record.value());
                       
                        // Msg JSon 변환
                        JsonReader reader_body = new JsonReader(new StringReader(message));
                        reader_body.setLenient(true);
                        ProgramMsg body = g.fromJson(reader_body, ProgramMsg.class);
                        body.setMsg_userid(body.getMsg_username());
                        
                        // 0. 값 초기화 
                        initModel (body);
  
                        // 게시판 처리
                        // 1. MMS URL 
                        log.info(body.toString());
                        if(body.getMsg_type().equals("MMO") && body.getMsg_com().equals("001")) {
                        	getMmsList (body);
                        	body.setMsg_type("2");
                        }
                        else {
                        	if(body.getAttachFileList() != null) {
                        		body.setMsg_type("2");
                        		body.setMsg_count(1);
                        		body.setMsg_mmsurl(body.getAttachFileList());
                        		
                        	}
                        	else { 
                        		body.setMsg_type("1");
                        	}
                        }
                        
                        // 2. 빈 메시지 처리 
                        if(body.getMsg_data().length() == 0 ) {
                        	body.setMsg_data("내용없음");
                        }

                        //3. 유저필터 
                        int msg_show = LimitWordUtils.getLimitUser(body.getPgm_key() , body.getMsg_com() , body.getMsg_userid());           
                        body.setMsg_show(msg_show);
                            
                        //4. 금칙어 필터 
                        if(body.getMsg_show() == 1 ) {
	                        for (LimitWord limitword : limitword_list) {
	                        	if( body.getPgm_key() == limitword.getPgm_key() ){
	                        		if(body.getMsg_data().contains(limitword.getWord_limt())) {
	                        			
	                        			log.info("Limit Word Data : " + body.getMsg_data());
	                        			body.setMsg_show(0);
	                        			body.setMsg_read(1);
	                        		}
	                        	}
	                        }
                        }
                        
                        programMsg_list.add(body);
                        // 수동으로 commit 및 seek 처리
                        currentOffsets.put(
                                new TopicPartition(record.topic(), record.partition()),
                                new OffsetAndMetadata(record.offset()+1, null));
                        
                        // 갑자기 죽을시 안되서 for 문 안에 넣어 본다. 
                        //consumer.commitSync(currentOffsets);
                    } catch(Exception e) {
                        log.error("Exception : ", e);
                    }
                 }
                
                
                //5. Insert
                if(programMsg_list.size() > 0) {
                	log.info("--" + programMsg_list.get(0).toString());
                	
                	BoardUtils.bulkInsert(programMsg_list);
                	programMsg_list.clear();
                }
                
                // 갑자기 죽을시 안되서 for 문 안에 넣어 본다. 
                consumer.commitAsync(currentOffsets, null);

                // Schedule ReLoad
                currentTime = (int) (System.currentTimeMillis() / 1000);
                if (currentTime - gapTime >= ReLoad_Sec) {
                    gapTime = currentTime;
                    
                    
                    // MO LIST 
                    ArrayList<String> mo_list = ScheduleUtils.getMOList();
                    if(!Arrays.equals(Monum.toArray(), mo_list.toArray())) {
                    	log.info(" Topic Change & Close & Re-Sub :  " + Monum.size());
                    	consumer.subscribe(mo_list, new HandleRebalance());
                        // insertTopic(mo_list);
                    	Monum = mo_list;
                    }
                    
                    // 금칙어 
                    limitword_list = LimitWordUtils.getLimitWord();
                    log.info(" Limit Word : " + limitword_list.size());
                    
                }

        	} 
        } catch (WakeupException e) {
            System.out.println("WakeupException");
        } catch(Exception e) {
            log.error(message, e);
        } finally {
            try {
                consumer.commitSync(currentOffsets);
            } finally {
                consumer.close();
    	        log.info("Exit thread : " + Thread.currentThread().getName());
            }
        }
    }

    private class ShutdownHook extends Thread {
        private Thread target;
        
        public ShutdownHook(Thread target, String name) {
            super(name);
            this.target = target;
        }
        
        public void run() {
            shutdown();
            
            try {
                //target 쓰레드가 종료될 때 까지 기다린다.
                target.join();                                
            } catch (InterruptedException e) {
                log.error("sth wrong in shutdown process", e);
            }
        }
    }
    
    @SuppressWarnings("unused")
    private void insertTopic(ArrayList<String> new_arr) {
        try {
            // insert topic
            Properties props = new Properties();
            props.put("bootstrap.servers", BootStrapServer);
    
            AdminClient adminClient = AdminClient.create(props);
    
            List<NewTopic> newTopics = new ArrayList<NewTopic>();
            for (String mo : new_arr) {
                newTopics.add(new NewTopic(mo, 2, (short)1));
            }
            log.info("insert " + new_arr.toString());
            adminClient.createTopics(newTopics);
            adminClient.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    private void getMmsList (ProgramMsg body) {
    	
        
    	try { 
	        String mms_tempurl_list = "";
	    	body.setAttachFileList( body.getAttachFileList().replace(Web_Dir, ""));
	    	ArrayList<String> mms_path = new ArrayList<String>();
	
	    	// Parsing  및 분리 
	    	StringTokenizer mms = new StringTokenizer(body.getAttachFileList(), "|");
	    	
	    	while(mms.hasMoreTokens()){
	    		mms_path.add(mms.nextToken());
	    	}
	    	
	    	for(int i = 0; i < mms_path.size() ; i++) {
	    		String mms_tempurl = " ";
	    		
	    		mms_tempurl = Web_Root;
	    		mms_tempurl += "/";
	    		mms_tempurl += mms_path.get(i);
	    		mms_tempurl_list += mms_tempurl;
	    		
	            if(mms_path.size()  > i + 1 ) {
	                mms_tempurl_list += "|";
	            }
	    	}
	    	log.info("MMS COUNT :" + mms_path.size() + " " + "LIST : " + mms_tempurl_list );
	    	
	    	body.setMsg_count(mms_path.size());
	    	body.setMsg_mmsurl(mms_tempurl_list);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    private void initModel (ProgramMsg body) {
        body.setMsg_show(1);
		body.setMsg_read(0);
		body.setCamp_key(0);
		body.setFeedid("");
		body.setCommentid("");
		body.setMsg_count(0);
		body.setMsg_mmsurl("");
		if(CommonUtils.checkEmpty(body.getMsg_title())) body.setMsg_title("");
		body.setMsg_userid(body.getMsg_username());
    }
}
