package net.infobank.itv.mo_board.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.infobank.itv.mo_board.dao.ScheduleDAO;
import net.infobank.itv.mo_common.model.Schedule;
import net.infobank.itv.mo_common.util.MyBatisConnectionFactory;

public class ScheduleUtils {
    private static Logger log = LoggerFactory.getLogger(ScheduleUtils.class);
    

    public static List<Schedule> getSchedule() {
        ScheduleDAO scheduleDAO = new ScheduleDAO(MyBatisConnectionFactory.getSqlSessionFactory());
        List<Schedule> list = scheduleDAO.selectSchedule();
        return list;
    }
    
    public static int getPgmKey(List<Schedule> pgm_codelist, String Mo, String ymd, String hm) {

        int pgm_key = 0;
        String[] weeks = { "일", "월", "화", "수", "목", "금", "토" };
        Calendar calendar = Calendar.getInstance();
        log.info( pgm_codelist.size() + Mo + ymd + hm );


         try {
             for (int i = 0; i < pgm_codelist.size(); i++) {
                 Schedule pgm = (Schedule) pgm_codelist.get(i);
                 String week;

                 // 시작 종료 비교
                 if (!Mo.equals(pgm.getPgm_mo()))
                     continue;


                 if (pgm.getPgm_sdate().compareTo(ymd) <= 0
                         && pgm.getPgm_edate().compareTo(ymd) > 0) {
                     if (pgm.getPgm_stm().compareTo(pgm.getPgm_etm()) < 0) {
                         // 24 시간
                         week = weeks[calendar.get(Calendar.DAY_OF_WEEK) - 1];
                         if ((pgm.getPgm_stm()).compareTo(hm) <= 0 && (pgm.getPgm_etm()).compareTo(hm) > 0) {
                             if (pgm.getPgm_week().indexOf(week) >= 0) {
                                 pgm_key = pgm.getPgm_key();
                                 log.info( "match : " +pgm_key);
                                 break;
                             }
                         }
                     } else {
                         // 48시간
                         if (calendar.get(Calendar.DAY_OF_WEEK) >= 2)
                             week = weeks[calendar.get(Calendar.DAY_OF_WEEK) - 2];
                         else
                             week = weeks[calendar.get(Calendar.DAY_OF_WEEK) + 5];

                         if ((pgm.getPgm_stm()).compareTo(hm) <= 0 || (pgm.getPgm_etm()).compareTo(hm) < 0) {
                             if (pgm.getPgm_week().indexOf(week) >= 0) {
                                 pgm_key = pgm.getPgm_key();
                                 log.info( "match : " +pgm_key);
                                 break;
                             }
                         }
                     }
                 }
             }
         } catch (Exception e) {
             log.error(hm, e);
         }

         return pgm_key;
     }
     
    public static ArrayList<String> getMOList() {
        ArrayList<String> mo_arr = new ArrayList<>();
        List<Schedule> list_mo = getMOSchedule();
        for (Schedule schedule : list_mo) {
            mo_arr.add(schedule.getPgm_mo().replace("#", ""));
        }
        return mo_arr;
    }

    public static List<Schedule> getMOSchedule() {
        ScheduleDAO scheduleDAO = new ScheduleDAO(MyBatisConnectionFactory.getSqlSessionFactory());
        List<Schedule> list = new ArrayList<Schedule>();
        try {
            list = scheduleDAO.selectMoSchedule();
        } catch (Exception e) {
            log.error("Exception getMOSchedule : ", e);
        }
        return list;
    }
}
