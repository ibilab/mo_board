package net.infobank.itv.mo_board;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import net.infobank.itv.mo_board.kafka.ConsumerThread;
import net.infobank.itv.mo_board.kafka.ConsumerThread2;
import net.infobank.itv.mo_board.util.ScheduleUtils;
import net.infobank.itv.mo_common.model.ConsumerInfo;
import net.infobank.itv.mo_common.util.FileConfig;
import net.infobank.itv.mo_common.util.MyBatisConnectionFactory;

@SpringBootApplication
public class Mo_Board {
    private static Logger log = LoggerFactory.getLogger(Mo_Board.class);

    public HashMap<String, Object> thread_list;
    public ConsumerInfo conInfo;

    public static void main(String[] args) {
        Mo_Board pgmAssign = new Mo_Board();
        pgmAssign.start(args);
    }

    public void start(String[] args) {
        Runtime.getRuntime().addShutdownHook(new ShutdownThread());
        FileConfig _config;

        // 접속정보 를 파일에서 가져온다.
        if (args.length != 1) {
            System.err.println("error: not enough argument");
            System.err.println("<arg1:config file>");
            return;
        }

        _config = new FileConfig(args[0]);

        String group_name = new Object() {}.getClass().getEnclosingClass().getSimpleName();
        MDC.put("log_file", _config.getLog_dir() + "/" + group_name+ "/" + group_name);
        
        Properties props = new Properties();
        props.put("driver", _config.getDriver());
        props.put("url", _config.getUrl());
        props.put("user", _config.getUser());
        props.put("password", _config.getPw());
        MyBatisConnectionFactory.setSqlSessionFactory(props);

        String mo_schedule_term = _config.getMoScheduleTerm(); // 스케쥴 확인 (초)
        String kafka_consumer_cnt = _config.getKafkaConsumerCnt(); // consumer 갯수
        int consumer_count = Integer.parseInt(kafka_consumer_cnt);
        
        String version = _config.getVersion(); // 버전 
        int nVersion = Integer.parseInt(version);

        conInfo = new ConsumerInfo();
        conInfo.setBoot_strap_server(_config.getKafkaBroker());
        conInfo.setPre_group_name(group_name);
        conInfo.setReload_sec(Integer.parseInt(mo_schedule_term));
        conInfo.setWeb_root(_config.getWeb_root());
        conInfo.setWeb_dir(_config.getWeb_dir()); 

        thread_list = new HashMap<String, Object>();

        ArrayList<String> new_arr = ScheduleUtils.getMOList();
        for (int i = 0; i < consumer_count; i++) {         
        	if (nVersion == 2) {
        		insertConsumerThread2(new_arr, i);
        	}
        	else {
        		insertConsumerThread(new_arr, i);
        	}
        }
    }
    
    @SuppressWarnings("unused")
    private void deleteTopic(ArrayList<String> new_arr) {
        try {
        // delete topic
            Properties props = new Properties();
            props.put("bootstrap.servers", conInfo.getBoot_strap_server());
    
            AdminClient adminClient = AdminClient.create(props);

            adminClient.deleteTopics(new_arr);
            adminClient.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    @SuppressWarnings("unused")
    private void insertTopic(ArrayList<String> new_arr) {
        try {
            // insert topic
            Properties props = new Properties();
            props.put("bootstrap.servers", conInfo.getBoot_strap_server());
    
            AdminClient adminClient = AdminClient.create(props);
    
            List<NewTopic> newTopics = new ArrayList<NewTopic>();
            for (String mo : new_arr) {
                newTopics.add(new NewTopic(mo, 2, (short)1));
            }
            log.info("insert " + new_arr.toString());
            adminClient.createTopics(newTopics);
            adminClient.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> diffArray(ArrayList<String> arr1, ArrayList<String> arr2) {
        ArrayList<String> list1 = new ArrayList<String>(arr1);
        ArrayList<String> list2 = new ArrayList<String>(arr2);
        list1.removeAll(list2);
        return list1;
    }

    public void insertConsumerThread(ArrayList<String> mo, int i) {
        conInfo.setMo_num(mo);

        Runnable consumer = new ConsumerThread(conInfo);
        Thread thread_consumer = new Thread(consumer);
        thread_consumer.start();

        log.info("id : " + thread_consumer.getId() + " name : " + thread_consumer.getName());
        thread_list.put(i + "", consumer);
    }
    
    public void insertConsumerThread2(ArrayList<String> mo, int i) {
        conInfo.setMo_num(mo);

        Runnable consumer = new ConsumerThread2(conInfo);
        Thread thread_consumer = new Thread(consumer);
        thread_consumer.start();

        log.info("~~~  VERSION 2 START");
        log.info("id : " + thread_consumer.getId() + " name : " + thread_consumer.getName());
        thread_list.put(i + "", consumer);
    }

    public class ShutdownThread extends Thread {
        public void run() {
            for (int i = 0; i < thread_list.size() ; i++) {
                ConsumerThread thread = (ConsumerThread) thread_list.get(i+"");
                thread.shutdown();
            }
        }
    }
    
    public void deleteConsumerThread(String no) {
        try {
            ConsumerThread thread_consumer = (ConsumerThread) thread_list.get(no);
            thread_consumer.shutdown();
            thread_list.remove(no);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
