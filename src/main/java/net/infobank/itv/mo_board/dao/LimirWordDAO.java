package net.infobank.itv.mo_board.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.infobank.itv.mo_board.model.LimitWord;

public class LimirWordDAO {
    private static Logger log = LoggerFactory.getLogger(LimirWordDAO.class);
	private SqlSessionFactory sqlSessionFactory = null;

	public LimirWordDAO(SqlSessionFactory sqlSessionFactory){
        this.sqlSessionFactory = sqlSessionFactory;
    }


	public List<LimitWord> selectLimitWord() {
		List<LimitWord> list = new ArrayList<LimitWord>();
		SqlSession session = sqlSessionFactory.openSession();

		try {
			list = session.selectList("LimitWord.selectLimitWord");
		} catch(Exception e)  {
		    log.error("LimitWord.selectLimitWord : " , e);
		} finally {
			session.close();
		}

		return list;
	}
	
	public List<LimitWord> selectLimitWord_2() {
		List<LimitWord> list = new ArrayList<LimitWord>();
		SqlSession session = sqlSessionFactory.openSession();

		try {
			list = session.selectList("LimitWord.selectLimitWord_2");
		} catch(Exception e)  {
		    log.error("LimitWord.selectLimitWord_2 : " , e);
		} finally {
			session.close();
		}

		return list;
	}
	
	public Integer selectLimitUser(String userTable , String msg_com , String msg_userid) {
		
		SqlSession session = sqlSessionFactory.openSession();
		
		
		Integer msg_show = 1;
		HashMap<String,String> map = new HashMap<String,String>();
		map.put("userTable", userTable);
		map.put("msg_com", msg_com);
		map.put("msg_userid", msg_userid);
		
		try {
			
			msg_show = session.selectOne("LimitWord.selectLimitUser",  map);
			
			if(msg_show == null )
				msg_show = 1;
			
		} catch(Exception e)  {
		    log.error("LimitWord.selectLimitUser : " , e);
		} finally {
			session.close();
		}

		return msg_show;
	}
	
	public Integer selectLimitUser_2(String userTable ,  String msg_userid) {
		
		SqlSession session = sqlSessionFactory.openSession();
		
		
		Integer msg_show = 1;
		HashMap<String,String> map = new HashMap<String,String>();
		map.put("userTable", userTable);
		map.put("msg_userid", msg_userid);
		
		try {
			
			msg_show = session.selectOne("LimitWord.selectLimitUser_2",  map);
			
			if(msg_show == null )
				msg_show = 1;
			
		} catch(Exception e)  {
		    log.error("LimitWord.selectLimitUser : " , e);
		} finally {
			session.close();
		}

		return msg_show;
	}

}
